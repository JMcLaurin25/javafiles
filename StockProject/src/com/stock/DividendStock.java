package com.stock;

public class DividendStock extends Stock {
		@Override
	public String toString() {
		return super.toString() + ", dividend of " + this.getDividend();
	}

		private double dividend;
		
		public DividendStock(String symbol, int shares, double stockPrice, double dividend) {
			super(symbol, shares, stockPrice);
			this.dividend = dividend;
		}
		
		public double getDividend() {
			return dividend;
		}

		public void setDividend(double dividend) {
			this.dividend = dividend;
		}
}
