package com.stock;

public class Stock {
	@Override
	public String toString() {
		return "You have bought " + this.getShares() + " shares of " + this.getSymbol() + " at $" + this.getStockPrice() + " per share"; 
	}
	private String symbol;
	private int shares;
	private double stockPrice;
	
	public Stock() {
		super();
		this.symbol = "";
		this.shares = 0;
		this.stockPrice = 0;
	}
	
	public Stock(String symbol, int shares, double stockPrice) {
		super();
		this.symbol = symbol;
		this.shares = shares;
		this.stockPrice = stockPrice;
	}
	
	//----------------------------
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	//----------------------------
	public int getShares() {
		return shares;
	}
	public void setShares(int shares) {
		this.shares = shares;
	}
	//----------------------------
	public double getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	//----------------------------
	
}
