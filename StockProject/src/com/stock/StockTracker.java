package com.stock;

import java.util.Collection;
import java.util.Scanner;

public class StockTracker {
	public static void main(String[] args) {
		Scanner scanSelect = new Scanner(System.in);
		printIntro();

		StockAccount account = collectAccountInfo();

		boolean Loop = true;

		while (Loop) {
			String selection = "";
			System.out.println("You can:\n\t(b)uy stock\n\t(s)ell stock\n\t(d)eposit\n\t(w)ithdraw\n\t(q)uit");
			System.out.println("Enter the first letter of your choice above and hit <ENTER>");
			selection = (scanSelect.next());
			String selLower = selection.toLowerCase();
			double curBalance = account.getBalance();

			System.out.println("\nselection entry is: " + selection);
			
			switch (selLower) {
			case ("b"):
				System.out.println("\nYou will now by stock for your account\n");
				Stock thisStock = collectStockInfo();
				isDividend(thisStock);
				try {
					account.buyStock(thisStock);
				} catch (StockException e) {
					System.out.println("There was an error buying stock");
					System.out.println(e.getMessage());
				}
				
				printAccountSummary(account);
				break;
			case ("s"):
				System.out.println("\nYou will now sell stock from your account\n");
				Stock saleStock = collectStockInfo();
				try {
					account.sellStock(saleStock);
				} catch (StockException e) {
					System.out.println("There was an error selling stock");
					System.out.println(e.getMessage());
				}
				
				
				printAccountSummary(account);
				break;
			case ("d"):
				deposit(scanSelect, account, curBalance);  
				break;
			case ("w"):
				withdraw(scanSelect, account, curBalance); 
				break;
			case ("q"):
				System.out.println("Thank you for using the Stock Tracker program");
				Loop = false;
				break;
			default:
				System.out.println("You have entered an incorrect option");
				break;
			}
		}
	}

	private static void isDividend(Stock thisStock) {
		Scanner divInput = new Scanner(System.in);

		System.out.println("Is the stock a dividend stock? y/n");
		String divStock = divInput.nextLine();
		String divStockLower = divStock.toLowerCase();
		
		boolean divCheck = true;
		
		while(divCheck){
			switch (divStockLower) {
			case ("y"):
				System.out.println("Enter the dividend value and hit <ENTER>");
				double dividendValue = divInput.nextDouble();
				
				DividendStock newDivStock = new DividendStock(thisStock.getSymbol(), thisStock.getShares(), thisStock.getStockPrice(), dividendValue);
	
				newDivStock.setDividend(dividendValue);
				
				divCheck = false;
				break;
			case ("n"):
				divCheck = false;
				break;
			default:
				System.out.println("Invalid Entry");
				divCheck = false;
				break;
			}		
		}
	}
	
	private static void deposit(Scanner scanSelect, StockAccount account,
			double curBalance) {
		System.out.println("\nYou will now deposit to your account\n");
		System.out.println("How much would you like to deposit?");
		double addBalance = (scanSelect.nextDouble());
		
		account.setBalance(addBalance + curBalance);
		
		printAccountSummary(account);
		pause();
	}

	public static void pause() {
		System.out.println("Press <ENTER> to continue");
		try {
		    System.in.read();
		}  
		catch(Exception e) {}
	}

	private static void withdraw(Scanner scanSelect, StockAccount account,
			double curBalance) {
		System.out.println("\nYou will now withdraw to your account\n");
		System.out.println("How much would you like to withdraw?");
		double subBalance = (scanSelect.nextDouble());
		if (subBalance > curBalance) {
			System.out.println("Insufficient funds");
		} else {
			account.setBalance(curBalance - subBalance);
			
			printAccountSummary(account);
		}
		pause();
	}
	
	private static Stock collectStockInfo() {
		Scanner stockInput = new Scanner(System.in);

		System.out.println("Please enter the stock symbol and hit <ENTER>");
		String symbol = stockInput.nextLine();

		System.out.println("Please enter the number of (whole) shares and hit <ENTER>");
		int shares = stockInput.nextInt();

		System.out.println("Please enter the price of the stock and hit <ENTER>");
		double stockPrice = stockInput.nextDouble();

		return new Stock(symbol, shares, stockPrice);
	}

	private static StockAccount collectAccountInfo() {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter your name and hit <ENTER>");
		String name = input.nextLine();

		System.out.println("Please enter your initial account balance and hit <ENTER>");
		double balance = input.nextDouble();

		if (balance < 0) {
			StockAccount account = new StockAccount(name);
			System.out.println("Negative balances are not allowed.\nAn account has been opened with "
							+ account.getBalance());

			return account;
		} else {
			return new StockAccount(name, balance);
		}
	}

	private static void printAccountSummary(StockAccount account) {
		System.out.println();
		System.out.println("Your account details:");
		System.out.println("Name: " + account.getName());
		System.out.println("Account Balance: " + account.getBalance());
		Collection<Stock> heldStock = account.getHeldStock();
		if (heldStock.isEmpty()) {
			System.out.println("You do not own any stock.");
		} else {
			System.out.println("You currently own the following stocks: ");
			for (Stock stock : heldStock) {
				System.out.println("- " + stock.toString());
			}
		}
		System.out.println();
	}

	private static void printIntro() {
		System.out.println("Welcome to the Stock Tracker Program!");
		System.out
				.println("This program will help you track information\nabout your investments\n");

	}

}